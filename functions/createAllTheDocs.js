const faunadb = require('faunadb')

const q = faunadb.query;
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET
});

exports.handler = async (event) => {
  const email = JSON.parse(event.body);

  try {
    const isUserChoiceDocExisted = await client.query(
      q.Exists(
        q.Match(
          q.Index('user_choice_by_email'),
          email
        )
      )
    );

    if (!isUserChoiceDocExisted) {
      await client.query(
        q.Create(
          q.Collection('UserChoice'),
          {
            data: {
              email: email,
              likes: [],
              favoritesID: []
            }
          }
        )
      );
    }

    const isFavoritesDocExisted = await client.query(
      q.Exists(
        q.Match(
          q.Index('favorites_by_emaill'),
          email
        )
      )
    );

    if (!isFavoritesDocExisted) {
      await client.query(
        q.Create(
          q.Collection('Favorites'),
          {
            data: {
              email: email,
              shows: []
            }
          }
        )
      );
    }

    return {
      statusCode: 200,
      body: JSON.stringify('All were created')
    };
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify(error)
    };
  }
};
