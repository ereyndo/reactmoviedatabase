const getFavoritesDocument = require('./getFavoritesDocument');

exports.handler = async (event) => {
  const email = JSON.parse(event.body);

  try {
    const favoritesDocument = await getFavoritesDocument(email);

    return {
      statusCode: 200,
      body: JSON.stringify(favoritesDocument.data.shows)
    };
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify(error)
    };
  }
}
