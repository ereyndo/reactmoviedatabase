const getLikeDocument = require('./getUserChoiceDocument');

exports.handler = async (event) => {
  const {email, showID} = JSON.parse(event.body);

  try {
    const likeDocument = await getLikeDocument(email);
    const likesList = likeDocument.data.likes;
    const favsIDList = likeDocument.data.favoritesID;

    return {
      statusCode: 200,
      body: JSON.stringify({
        isLiked: likesList.includes(showID),
        isFavorite: favsIDList.includes(showID)
      })
    };
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify(error)
    };
  }
};
