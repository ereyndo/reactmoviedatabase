const faunadb = require('faunadb');
const getUserChoiceDocument = require('./getUserChoiceDocument');
const getFavoritesDocument = require('./getFavoritesDocument');

const q = faunadb.query;
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET
});

exports.handler = async (event) => {
  const {email, currentShow, operation} = JSON.parse(event.body);

  try {
    const userChoiceDocument = await getUserChoiceDocument(email);
    const favoritesIDList = userChoiceDocument.data.favoritesID;

    const favoritesDocument = await getFavoritesDocument(email);
    const favoritesShowList = favoritesDocument.data.shows;

    if (operation === 'add') {
      favoritesIDList.push(currentShow.id);
      favoritesShowList.push(currentShow);
    } else {
      favoritesIDList.splice(favoritesIDList.indexOf(currentShow.id), 1);
      favoritesShowList.splice(favoritesShowList.indexOf(
          favoritesShowList.find(show => show.id === currentShow.id)
      ), 1);
    }

    await client.query(
      q.Update(
        userChoiceDocument.ref,
        {
          data: {
            email: email,
            favoritesID: [...favoritesIDList]
          }
        }
      )
    );

    await client.query(
      q.Update(
        favoritesDocument.ref,
        {
          data: {
            email: email,
            shows: [...favoritesShowList]
          }
        }
      )
    );

    return {
      statusCode: 200,
      body: JSON.stringify('UserChoice and Favorites documents were changed!')
    };
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify(error)
    };
  }
}
