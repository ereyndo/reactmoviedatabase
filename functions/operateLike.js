const faunadb = require('faunadb')
const getUserChoiceDocument = require('./getUserChoiceDocument');

const q = faunadb.query;
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET
});

exports.handler = async (event) => {
  const {email, showID, operation} = JSON.parse(event.body);

  try {
    const userChoiceDocument = await getUserChoiceDocument(email);
    const likesList = userChoiceDocument.data.likes;

    if (operation === 'add') {
      likesList.push(showID);
    } else {
      likesList.splice(likesList.indexOf(showID), 1);
    }

    const response = await client.query(
      q.Update(
        userChoiceDocument.ref,
        {
          data: {
            email: email,
            likes: [...likesList]
          }
        }
      )
    );

    return {
      statusCode: 200,
      body: JSON.stringify(response)
    };
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify(error)
    };
  }
}
