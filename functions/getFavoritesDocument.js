const faunadb = require('faunadb')

const q = faunadb.query;
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET
});

module.exports = async (email) => {
  try {
    return await client.query(
      q.Get(
        q.Match(
          q.Index('favorites_by_emaill'),
          email
        )
      )
    );
  } catch (error) {
    return error;
  }
}