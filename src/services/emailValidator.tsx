export const emailValidator = (email: string | undefined): boolean => {
  const regExp = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/;
  return email ? regExp.test(email) : false;
};
