import {Show} from 'components/Content/Library';

type CreateAllTheDocsProps = string | Error;
type CheckUserChoiceResponse = {
  isLiked: boolean,
  isFavorite: boolean
} | Error;
type GetFavoritesResponse = Show[] | Error;

export const createAllTheDocs = async (email: string): Promise<CreateAllTheDocsProps> => {
  const response = await fetch('/api/createAllTheDocs', {
    body: JSON.stringify(email),
    method: 'POST'
  });
  return response.json();
}

export const checkUserChoice = async (email: string, showID: number): Promise<CheckUserChoiceResponse> => {
  const response = await fetch('/api/checkUserChoice', {
    body: JSON.stringify({email, showID}),
    method: 'POST'
  });
  return response.json();
};

export const addLike = async (email: string, showID: number): Promise<any> => {
  const response = await fetch('/api/operateLike', {
    body: JSON.stringify({email, showID, operation: 'add'}),
    method: 'POST'
  });
  return response.json();
};

export const deleteLike = async (email: string, showID: number): Promise<any> => {
  const response = await fetch('/api/operateLike', {
    body: JSON.stringify({email, showID, operation: 'delete'}),
    method: 'POST'
  });
  return response.json();
};

export const addFavorite = async (email: string, show: Show): Promise<any> => {
  const response = await fetch('/api/operateFavorites', {
    body: JSON.stringify({email, currentShow: show, operation: 'add'}),
    method: 'POST'
  });
  return response.json();
};

export const deleteFavorite = async (email: string, show: Show): Promise<any> => {
  const response = await fetch('/api/operateFavorites', {
    body: JSON.stringify({email, currentShow: show, operation: 'delete'}),
    method: 'POST'
  });
  return response.json();
};

export const getFavorites = async (email: string): Promise<GetFavoritesResponse> => {
  const response = await fetch('/api/getFavoritesHandler', {
    body: JSON.stringify(email),
    method: 'POST'
  });
  return response.json();
};
