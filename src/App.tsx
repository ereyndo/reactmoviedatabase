import React, {useEffect, useState} from 'react';
import {Header} from 'components/Header';
import {Content} from 'components/Content';
import {Footer} from 'components/Footer';
import styles from 'app.module.scss';
import {BrowserRouter as Router} from 'react-router-dom';
import {createAllTheDocs} from 'services/backEndAPI';
import {Modal} from 'components/Modals';
import {StatusWindow} from 'components/Modals/StatusWindow';
import {SignInUp} from 'components/Modals/SignInUp';
import {useAppDispatch, useAppSelector} from 'hooks/reduxHooks';
import {
  auth,
  changeAuthStatusAndCurrentUser,
  selectAuthStatus,
  selectCurrentUser,
} from 'slices/commonSlice';
import {selectIsSignIn, selectIsSignModal} from './slices/unAuthUserSlice';

export type StatusMessage = {
  displayError: boolean,
  setDisplayError: (displayError: boolean) => void,
  sentConfirmation: boolean,
  setSentConfirmation: (sentConfirmation: boolean) => void,
}

const App = () => {
  const dispatch = useAppDispatch();
  const currentUser = useAppSelector(selectCurrentUser);
  const authStatus = useAppSelector(selectAuthStatus);
  const isSignModal = useAppSelector(selectIsSignModal);
  const isSignIn = useAppSelector(selectIsSignIn);
  const [displayError, setDisplayError] = useState<boolean>(false);
  const [sentConfirmation, setSentConfirmation] = useState<boolean>(false);

  useEffect(() => {
      async function fetchMyAPI() {
        if (currentUser) {
          try {
            await createAllTheDocs(currentUser.email);
          } catch (error) {
            // have to somehow process an error
          }
        }
      }

      fetchMyAPI();
    },
    [currentUser]);

  if (!authStatus) {
    if (window.location.hash && window.location.hash.indexOf('#confirmation_token=') === 0) {
      const token = window.location.hash.replace('#confirmation_token=', '');
      auth.confirm(token, true)
        .then(() => dispatch(changeAuthStatusAndCurrentUser(true)))
        .catch(() => {
          // have to somehow process an error
        });
    }
  }

  return (
    <Router>
      <div className={styles.mainContainer}>
        <div className={`${styles.main} ${authStatus ? styles.indentForAuth : null}`}>
          <Header authStatus={authStatus}/>
          <Content authStatus={authStatus}/>
          <Footer authStatus={authStatus}/>
        </div>
      </div>
      {
        isSignModal
        &&
        <Modal>
          <SignInUp statusMessage={{displayError, setDisplayError, sentConfirmation, setSentConfirmation}}/>
        </Modal>
      }
      {
        displayError
        &&
        <Modal>
          <StatusWindow message={isSignIn ? 'Failed to sign in' : 'Failed to sign up'} isError={true}/>
        </Modal>
      }
      {
        sentConfirmation
        &&
        <Modal>
          <StatusWindow message="Confirm your email address" isError={false}/>
        </Modal>
      }
    </Router>
  );
};

export default App;
