import {configureStore} from '@reduxjs/toolkit';
import unAuthUserSlice from './slices/unAuthUserSlice';
import commonSlice from './slices/commonSlice';

const store = configureStore({
  reducer: {
    common: commonSlice,
    unAuthUser: unAuthUserSlice
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: ['common/setCurrentUser'],
        ignoredPaths: ['common.currentUser']
      },
    }),
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
