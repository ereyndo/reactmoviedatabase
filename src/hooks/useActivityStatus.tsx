import React, {useEffect, useState} from 'react';
import {Nullable} from 'tsHelper';

export function useActivityStatus(mainElemRef: React.MutableRefObject<Nullable<HTMLDivElement>>, clickableElemRef: React.MutableRefObject<Nullable<HTMLElement>>): boolean {
  const [isActive, setActive] = useState<boolean>(false);

  useEffect(() => {
    const mainElem : Nullable<HTMLDivElement> = mainElemRef.current;
    const clickableElem: Nullable<HTMLElement> = clickableElemRef.current;

    const handleClickInside = (): void => {
      setActive(prevState => !prevState);
    };

    const handleClickOutside = (event: MouseEvent): void => {
      if (mainElem && !mainElem.contains(event.target as Node) && isActive) {
        setActive(false);
      }
    };

    clickableElem?.addEventListener('click', handleClickInside);

    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      clickableElem?.removeEventListener('click', handleClickInside);
      document.removeEventListener('mousedown', handleClickOutside);
    };
  });

  return isActive;
}
