import React from 'react';
import {Introduction} from './Introduction';
import {Library} from './Library';
import {Routes, Route, Navigate} from 'react-router-dom';

type ContentProps = {
  authStatus: boolean
};

export const Content = ({authStatus}: ContentProps) => {
  return (
    <div>
      {
        <Routes>
          {
            !authStatus ?
              <>
                <Route path="/" element={<Introduction/>}/>
                <Route path="*" element={<Navigate to="/"/>}/>
              </>
              :
              <>
                <Route path="/" element={<Library pageName="library"/>}/>
                <Route path="/my-list" element={<Library pageName="my-list"/>}/>
                <Route path="/search/:searchQuery" element={<Library pageName="search"/>}/>
                <Route path="*" element={<Navigate to="/"/>}/>
              </>
          }
        </Routes>
      }
    </div>
  );
};
