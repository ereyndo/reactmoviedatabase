import React, {useEffect, useState} from 'react';
import {MovieCard} from './MovieCard';
import {Filter} from './Filter';
import styles from './library.module.scss';
import {getFavorites} from 'services/backEndAPI';
import {Nullable} from 'tsHelper';
import {useParams} from 'react-router-dom';
import {useAppSelector} from 'hooks/reduxHooks';
import {selectCurrentUser} from 'slices/commonSlice';

type LibraryProps = {
  pageName: string
};
export type Show = {
  id: number,
  name: string,
  image?: {
    medium: string
  },
  summary: Nullable<string>,
  status: string,
  type: string,
  genres: string[],
  _links: {
    self: {
      href: string
    },
    previousepisode?: {
      href: string
    }
  }
};
export type ExtendedShow = {
  score: number,
  show: Show
}
type SetInitListOfShows = (initListOfShows: Show[]) => void;

const basicShowListLink = 'https://api.tvmaze.com/shows?page=0';

async function handleTransitionToLibrary(setInitListOfShows: SetInitListOfShows) {
  const basicShowList = localStorage.getItem('basicShowList');
  if (basicShowList) {
    setInitListOfShows(JSON.parse(basicShowList));
  } else {
    const response = await fetch(basicShowListLink);
    if (response.ok) {
      const json = await response.json();
      localStorage.setItem('basicShowList', JSON.stringify(json));
      setInitListOfShows(json);
    }
  }
}

async function handleTransitionToSearched(searchQuery: Nullable<string>, setInitListOfShows: SetInitListOfShows) {
  if (searchQuery) {
    const searchQueryValues = searchQuery.replace(/^search_query=/, '')
    const response = await fetch(`https://api.tvmaze.com/search/shows?q=${searchQueryValues}`);
    if (response.ok) {
      const json = await response.json();
      const parsedJson = json.map((el: ExtendedShow) => el.show);
      setInitListOfShows(parsedJson);
    }
  }
}

export const Library = ({pageName}: LibraryProps) => {
  const currentUser = useAppSelector(selectCurrentUser);
  const [initListOfShows, setInitListOfShows] = useState<Show[]>([]);
  const [listOfShows, setListOfShows] = useState<Show[]>([]);
  const [shownShows, setShownShows] = useState<Show[]>([]);
  const [needToReset, setNeedToReset] = useState<boolean>(false);
  const params = useParams();

  useEffect(() => {
      setNeedToReset(true);
    },
    [currentUser, params.searchQuery, pageName]);

  useEffect(() => {
      async function fetchMyAPI(): Promise<void> {
        try {
          if (pageName === 'my-list' && currentUser) {
            const favoritesShows = await getFavorites(currentUser.email);
            if (!(favoritesShows instanceof Error)) {
              setInitListOfShows(favoritesShows);
            }
          }
        } catch (error) {
          // have to somehow process an error
        }
      }

      fetchMyAPI();
    },
    [currentUser, pageName]
  );

  useEffect(() => {
      async function fetchMyAPI(): Promise<void> {
        try {
          if (pageName === 'library') {
            await handleTransitionToLibrary(setInitListOfShows);
          }
        } catch (error) {
          // have to somehow process an error
        }
      }

      fetchMyAPI();
    },
    [pageName]
  );

  useEffect(() => {
      async function fetchMyAPI(): Promise<void> {
        try {
          if (pageName === 'search') {
            await handleTransitionToSearched(params.searchQuery ?? null, setInitListOfShows);
          }
        } catch (error) {
          // have to somehow process an error
        }
      }

      fetchMyAPI();
    },
    [params.searchQuery, pageName]
  );

  useEffect(() => {
      setListOfShows(JSON.parse(JSON.stringify(initListOfShows)));
    },
    [initListOfShows]
  );

  useEffect(() => {
      setShownShows(listOfShows.slice(0, 20));
    },
    [listOfShows]
  );

  function handleLoadMore() {
    setShownShows(prev => prev.concat(listOfShows.slice(prev.length, prev.length + 20)));
  }

  return (
    <div className={styles.libraryContainer}>
      <Filter setListOfShows={setListOfShows} initListOfShows={initListOfShows} needToReset={needToReset}
              setNeedToReset={setNeedToReset}/>
      <div className={styles.library}>
        <div className={styles.movieContainer}>
          {
            shownShows.map((el, id) => <MovieCard key={id} show={el}/>)
          }
        </div>
        <button className={styles.loadBtn} onClick={handleLoadMore}>Load more</button>
      </div>
    </div>
  );
};
