import React, {useRef, useState} from 'react';
import {MovieDescription} from './MovieDescription';
import {useActivityStatus} from 'hooks/useActivityStatus';
import styles from './movieCard.module.scss';
import posterPlaceholder from 'assets/images/posterPlaceholder.jpg';
import {Show} from '../Library';
import {Nullable} from 'tsHelper';

type MovieCardProps = {
  show: Show
};

export const MovieCard = ({show}: MovieCardProps) => {
  const [movieDescriptionHeight, setMovieDescriptionHeight] = useState<number>(0);

  const movieCard = useRef<Nullable<HTMLDivElement>>(null);
  const moviePoster = useRef<Nullable<HTMLImageElement>>(null);
  const triangle = useRef<Nullable<HTMLDivElement>>(null);

  const isCardActive = useActivityStatus(movieCard, moviePoster);

  return (
    <div className={styles.movieCard} style={isCardActive ? {marginBottom: movieDescriptionHeight} : undefined}
         ref={movieCard}>
      <img src={show.image ? show.image.medium : posterPlaceholder} alt={show.name} className={styles.moviePoster} ref={moviePoster}/>
      {
        isCardActive ?
          <>
            <div className={styles.triangle} ref={triangle}/>
            <MovieDescription show={show} setMovieDescriptionHeight={setMovieDescriptionHeight}/>
          </>
          :
          null
      }
    </div>
  );
};
