import React, {useEffect, useState} from 'react';
import styles from './likeListTools.module.scss';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faThumbsUp, faList} from '@fortawesome/free-solid-svg-icons';
import {
  checkUserChoice,
  addLike,
  deleteLike,
  addFavorite,
  deleteFavorite
} from 'services/backEndAPI';
import {Show} from '../../../Library';
import {Nullable} from 'tsHelper';
import {useAppSelector} from 'hooks/reduxHooks';
import {selectCurrentUser} from 'slices/commonSlice';

type LikeListToolsProps = {
  show: Show
};

export const LikeListTools = ({show}: LikeListToolsProps) => {
  const currentUser = useAppSelector(selectCurrentUser);
  const [isAddToListActive, setAddToListActive] = useState<boolean>(false);
  const [changeAddToList, setChangeAddToList] = useState<Nullable<boolean>>(null);
  const [isThumbsUpActive, setThumbsUpActive] = useState<boolean>(false);
  const [changeThumbsUp, setChangeThumbsUp] = useState<Nullable<boolean>>(null);

  useEffect(() => {
    async function fetchMyAPI(): Promise<void> {
      if (currentUser) {
        const userChoice = await checkUserChoice(currentUser.email, show.id);
        if (!(userChoice instanceof Error)) {
          try {
            setThumbsUpActive(userChoice.isLiked);
            setAddToListActive(userChoice.isFavorite!);
          } catch (error) {
            // have to somehow process an error
          }
        }
      }
    }

    fetchMyAPI();
  }, [currentUser, show.id]);

  useEffect(() => {
    async function fetchMyAPI(): Promise<void> {
      if (changeThumbsUp === null) {
        return;
      }
      setThumbsUpActive(changeThumbsUp);

      if (currentUser) {
        try {
          if (changeThumbsUp) {
            await addLike(currentUser.email, show.id);
          } else {
            await deleteLike(currentUser.email, show.id);
          }
        } catch (error) {
          // have to somehow process an error
        }
      }
    }

    fetchMyAPI();
  }, [changeThumbsUp, currentUser, show.id]);

  useEffect(() => {
    async function fetchMyAPI(): Promise<void> {
      if (changeAddToList === null) {
        return;
      }
      setAddToListActive(changeAddToList);

      if (currentUser) {
        try {
          if (changeAddToList) {
            await addFavorite(currentUser.email, show);
          } else {
            await deleteFavorite(currentUser.email, show);
          }
        } catch (error) {
          // have to somehow process an error
        }
      }
    }

    fetchMyAPI();
  }, [changeAddToList, currentUser, show]);

  return (
    <div className={styles.likeListTools}>
      <p className={styles.addToList} onClick={() => {
        setChangeAddToList(!isAddToListActive)
      }}>
        <FontAwesomeIcon icon={faList}
                         className={`${styles.faList} ${isAddToListActive ? styles.faListActive : null}`}/>
        <span className={styles.textFaList}>Add to List</span>
      </p>
      <FontAwesomeIcon icon={faThumbsUp}
                       className={`${styles.faThumbsUp} ${isThumbsUpActive ? styles.faThumbsUpActive : null}`}
                       onClick={() => {
                         setChangeThumbsUp(!isThumbsUpActive);
                       }}/>
    </div>
  );
};
