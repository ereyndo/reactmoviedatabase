import React, {Dispatch, SetStateAction, useEffect, useRef, useState} from 'react';
import {LikeListTools} from './LikeListTools';
import styles from './movieDescription.module.scss';
import placeholder from 'assets/images/episodePlaceholder.png';
import {Show} from '../../Library';
import {Nullable} from 'tsHelper';

type MovieDescriptionProps = {
  show: Show,
  setMovieDescriptionHeight: Dispatch<SetStateAction<number>>
};
type FirstEpisode = {
  image: {
    original?: string;
  } | null
};

export const MovieDescription = ({show, setMovieDescriptionHeight}: MovieDescriptionProps) => {
  const [lastEpisodeImg, setLastEpisodeImg] = useState<Nullable<string>>(null);

  const movieDescription = useRef<HTMLDivElement>(null);

  useEffect(
    () => {
      if (movieDescription.current) {
        setMovieDescriptionHeight(parseFloat(window.getComputedStyle(movieDescription.current).height));
      }
    },
    [setMovieDescriptionHeight]
  );

  useEffect(
    () => {
      const fetchData = async (): Promise<void> => {
        try {
          if (show._links?.previousepisode) {
            const response = await fetch(`https://api.tvmaze.com/shows/${show.id}/episodebynumber?season=1&number=1`);

            if (response.ok) {
              const firstEpisode: FirstEpisode = await response.json();
              setLastEpisodeImg(firstEpisode.image?.original ?? null);
            }
          }
        } catch (e) {
          // have to somehow process an error
        }
      };

      fetchData();
    },
    [show.id, show._links]
  );

  return (
    <div className={styles.movieDescription} ref={movieDescription}>
      <div className={styles.contentContainer}>
        <img className={styles.backgroundImg} src={lastEpisodeImg ?? placeholder} alt="Some episode frame"/>
        <div className={styles.content}>
          <h2 className={styles.showName}>{show.name}</h2>
          {
            show.summary ?
              <div dangerouslySetInnerHTML={{__html: show.summary}} className={styles.summary}/>
              :
              null
          }
          <LikeListTools show={show}/>
        </div>
      </div>
    </div>
  );
};
