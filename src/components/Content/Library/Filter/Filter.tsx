import React, {Dispatch, SetStateAction, useEffect, useRef, useState} from 'react';
import styles from './filter.module.scss';
import {Show} from '../Library';
import {Nullable} from 'tsHelper';

type FilterProps = {
  setListOfShows: Dispatch<SetStateAction<Show[]>>,
  initListOfShows: Show[],
  needToReset: boolean,
  setNeedToReset: Dispatch<SetStateAction<boolean>>
};
type HandleFilterProps = {
  setListOfShows: Dispatch<SetStateAction<Show[]>>,
  initListOfShows: Show[],
  showType: React.MutableRefObject<Nullable<HTMLSelectElement>>,
  showStatus: React.MutableRefObject<Nullable<HTMLSelectElement>>,
  genres: React.MutableRefObject<Nullable<HTMLSelectElement>>
};

function handleFilter({setListOfShows, initListOfShows, showType, showStatus, genres}: HandleFilterProps) {
  setListOfShows(initListOfShows.filter(el => {
    const show = el;
    if (showStatus.current?.value) {
      if (showStatus.current.value !== show.status) {
        return false;
      }
    }
    if (showType.current?.value) {
      if (showType.current.value !== show.type) {
        return false;
      }
    }
    if (genres.current?.value) {
      if (!show.genres.includes(genres.current.value)) {
        return false;
      }
    }
    return true;
  }));
}

export const Filter = ({setListOfShows, initListOfShows, needToReset, setNeedToReset}: FilterProps) => {
  const [isFilterSidebar, setIsFilterSidebar] = useState<boolean>(false);
  const [closeTransition, setCloseTransition] = useState<boolean>(false);

  const showStatus = useRef<Nullable<HTMLSelectElement>>(null);
  const showType = useRef<Nullable<HTMLSelectElement>>(null);
  const genres = useRef<Nullable<HTMLSelectElement>>(null);

  useEffect(() => {
    if (needToReset && showStatus.current && showType.current && genres.current) {
      showStatus.current.selectedIndex = 0;
      showType.current.selectedIndex = 0;
      genres.current.selectedIndex = 0;
      setNeedToReset(false);
    }
  },
    [needToReset, setNeedToReset]);

  useEffect(() => {
    if (closeTransition) {
      setTimeout(() => {
        setIsFilterSidebar(false);
        setCloseTransition(false);
      }, 500);
    }
  },
    [closeTransition])

  return (
    <>
      <button className={styles.filterToggle} onClick={() => setIsFilterSidebar(true)}>
        Filters
      </button>
      <div className={`${styles.filterContainer} ${isFilterSidebar && styles.filterContainer_visible}`}>
        <div className={`${styles.filter} ${isFilterSidebar && !closeTransition && styles.filter_visible}`}>
          <div className={styles.closeBtn} onClick={() => setCloseTransition(true)} />
          <div className={styles.selectContainer}>
            <label htmlFor="status" className={styles.label}>Show Status</label>
            <select id="status" className={styles.select} ref={showStatus}>
              <option value=""/>
              <option value="Running">Running</option>
              <option value="Ended">Ended</option>
              <option value="To Be Determined">To Be Determined</option>
              <option value="In Development">In Development</option>
            </select>
          </div>
          <div className={styles.selectContainer}>
            <label htmlFor="type" className={styles.label}>Show Type</label>
            <select id="type" className={styles.select} ref={showType}>
              <option value=""/>
              <option value="Scripted">Scripted</option>
              <option value="Animation">Animation</option>
              <option value="Reality">Reality</option>
              <option value="Talk Show">Talk Show</option>
              <option value="Documentary">Documentary</option>
              <option value="Game Show">Game Show</option>
              <option value="News">News</option>
              <option value="Sports">Sports</option>
              <option value="Variety">Variety</option>
              <option value="Award Show">Award Show</option>
              <option value="Panel Show">Panel Show</option>
            </select>
          </div>
          <div className={styles.selectContainer}>
            <label htmlFor="genres" className={styles.label}>Genre</label>
            <select id="genres" className={styles.select} ref={genres}>
              <option value=""/>
              <option value="Action">Action</option>
              <option value="Adult">Adult</option>
              <option value="Adventure">Adventure</option>
              <option value="Anime">Anime</option>
              <option value="Children">Children</option>
              <option value="Comedy">Comedy</option>
              <option value="Crime">Crime</option>
              <option value="DIY">DIY</option>
              <option value="Drama">Drama</option>
              <option value="Espionage">Espionage</option>
              <option value="Family">Family</option>
              <option value="Fantasy">Fantasy</option>
              <option value="Food">Food</option>
              <option value="History">History</option>
              <option value="Horror">Horror</option>
              <option value="Legal">Legal</option>
              <option value="Music">Music</option>
              <option value="Mystery">Mystery</option>
              <option value="Nature">Nature</option>
              <option value="Romance">Romance</option>
              <option value="Science-Fiction">Science-Fiction</option>
              <option value="Sports">Sports</option>
              <option value="Supernatural">Supernatural</option>
              <option value="Thriller">Thriller</option>
              <option value="Travel">Travel</option>
              <option value="War">War</option>
              <option value="Western">Western</option>
            </select>
          </div>
          <button className={styles.filterBtn} onClick={() => handleFilter({setListOfShows, initListOfShows, showType, showStatus, genres})}>
            Filter
          </button>
        </div>
      </div>
    </>
  )
};
