import React from 'react';
import {InfoBlock} from './InfoBlock';
import {GetStarted} from 'components/GetStarted';
import shangChiImg from 'assets/images/shangChi.webp';
import gadgetsImg from 'assets/images/gadgets.webp';
import friendsImg from 'assets/images/friends.webp';
import styles from './introduction.module.scss';

export const Introduction = () => {
  return (
    <>
      <InfoBlock
        side='leftText'
        img={{
          src: shangChiImg,
          alt: 'Shang Chi movie.'
        }}
        text={{
          header: 'Stay up to date.',
          description: 'All the new content is arisen here as fast as possible.'
        }}
      />
      <InfoBlock
        side='rightText'
        img={{
          src: gadgetsImg,
          alt: 'Different gadgets with some movies.'
        }}
        text={{
          header: 'Watch everywhere.',
          description: 'You can use your phone, tablet, laptop and more to watch your favorites.'
        }}
      />
      <InfoBlock
        side='leftText'
        img={{
          src: friendsImg,
          alt: 'Friends in the cinema.'
        }}
        text={{
          header: 'Share with your friends.',
          description: 'You can share with your friends your preferences.'
        }}
      />
      <div className={styles.signUp}>
        <GetStarted/>
      </div>
    </>
  );
};
