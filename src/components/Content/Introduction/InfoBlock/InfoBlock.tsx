import React from 'react';
import styles from './infoBlock.module.scss';

type InfoBlockProps = {
    img: {
        src: string,
        alt: string
    },
    text: {
        header: string,
        description: string
    },
    side: string
};

export const InfoBlock = ({img: {src, alt}, text: {header, description}, side}: InfoBlockProps) => {
  return (
    <div className={`${styles[side]} ${styles.infoBlock}`}>
      <div className={styles.imgContainer}>
        <img src={src} alt={alt} className={styles.img}/>
      </div>
      <div className={styles.textContainer}>
        <div className={styles.text}>
          <h2 className={styles.header}>{header}</h2>
          <p className={styles.description}>{description}</p>
        </div>
      </div>
    </div>
  );
};
