import React, {useEffect, useRef} from 'react';
import styles from './getStarted.module.scss';
import {emailValidator} from 'services/emailValidator';
import {Nullable} from 'tsHelper';
import {useAppDispatch, useAppSelector} from 'hooks/reduxHooks';
import {
  selectIsInputEmpty,
  selectIsSignModal,
  setEmail,
  setIsInputEmpty,
  setIsSignIn,
  setIsSignModal
} from 'slices/unAuthUserSlice';

export const GetStarted = () => {
  const dispatch = useAppDispatch();
  const isSignModal = useAppSelector(selectIsSignModal);
  const isInputEmpty = useAppSelector(selectIsInputEmpty);

  const enteredEmail = useRef<Nullable<HTMLInputElement>>(null);

  useEffect(() => {
    if (isInputEmpty && !isSignModal) {
      if (enteredEmail.current) {
        enteredEmail.current.value = '';
      }
      dispatch(setIsInputEmpty(false));
    }
  }, [isInputEmpty, isSignModal, dispatch]);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
    event.preventDefault();
    const enteredEmailCurrent: Nullable<HTMLInputElement> = enteredEmail.current;
    if (emailValidator(enteredEmailCurrent?.value)) {
      dispatch(setIsSignModal(true));
      dispatch(setIsSignIn(false));
      dispatch(setEmail(enteredEmail.current?.value ?? ''));
    } else {
      if (enteredEmailCurrent) {
        enteredEmailCurrent.style.color = 'red';
      }
    }
  };

  const handleInput = (): void => {
    const enteredEmailCurrent: Nullable<HTMLInputElement> = enteredEmail.current;
    enteredEmailCurrent!.style.color = '';
  };

  return (
    <div>
      <form className={styles.form}>
        <label htmlFor="email" className={styles.suggestion}>Don't have a profile? Just enter your email to get
          started.</label>
        <div className={styles.inputBlock}>
          <input type="email" name="email" placeholder="Your email" className={styles.inputEmail}
                 ref={enteredEmail} onChange={handleInput} required/>
          <button className={styles.btn} onClick={handleClick}>Let's Go</button>
        </div>
      </form>
    </div>
  );
};
