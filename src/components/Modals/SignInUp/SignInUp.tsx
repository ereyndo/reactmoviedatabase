import React, {useEffect, useRef, useState} from 'react';
import modalStyles from '../modal.module.scss';
import signInUpStyles from './signInUp.module.scss';
import { StatusMessage } from 'App';
import {Nullable} from 'tsHelper';
import {auth, changeAuthStatusAndCurrentUser} from 'slices/commonSlice';
import {useAppDispatch, useAppSelector} from 'hooks/reduxHooks';
import {selectEmail, selectIsSignIn, setIsInputEmpty, setIsSignModal} from 'slices/unAuthUserSlice';
import {AppDispatch} from 'store';

type ActionsBeforeWillUnmountArgs = {
  isSignIn: boolean,
  dispatch: AppDispatch,
  sentConfirmation: boolean,
};
type SignInUpProps = {
  statusMessage: StatusMessage,
};

function actBeforeWillUnmount({isSignIn, dispatch, sentConfirmation}: ActionsBeforeWillUnmountArgs): void {
  if (!isSignIn && sentConfirmation) {
    dispatch(setIsInputEmpty(true));
  }
  dispatch(setIsSignModal(false));
}

export const SignInUp = ({statusMessage}: SignInUpProps) => {
  const dispatch = useAppDispatch();
  const isSignIn = useAppSelector(selectIsSignIn);
  const email = useAppSelector(selectEmail);
  const [readyForClose, setReadyForClose] = useState<boolean>(false);

  const modal = useRef<Nullable<HTMLDivElement>>(null);

  useEffect(() => {
    if (readyForClose) {
      actBeforeWillUnmount({isSignIn, dispatch, sentConfirmation: statusMessage.sentConfirmation});
    }
  }, [readyForClose, isSignIn, dispatch, statusMessage.sentConfirmation]);

  const handleClickCloseBtn = (): void => {
    actBeforeWillUnmount({isSignIn, dispatch, sentConfirmation: statusMessage.sentConfirmation});
  };

  const handleSignUp = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault();
    try {
      const eventTarget = event.target as HTMLFormElement;
      await auth.signup(eventTarget.email.value, eventTarget.password.value);
      statusMessage.setSentConfirmation(true);
      setReadyForClose(true);
      setTimeout(() => {
        statusMessage.setSentConfirmation(false);
      }, 2000);
    } catch (error) {
      statusMessage.setDisplayError(true);
      setTimeout(() => {
        statusMessage.setDisplayError(false);
      }, 2000);
    }
  }

  const handleSignIn = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault();
    try {
      const eventTarget = event.target as HTMLFormElement;
      await auth.login(eventTarget.email.value, eventTarget.password.value, true);
      dispatch(changeAuthStatusAndCurrentUser(true));
      dispatch(setIsSignModal(false));
    } catch (error) {
      statusMessage.setDisplayError(true);
      setTimeout(() => {
        statusMessage.setDisplayError(false);
      }, 2000);
    }
  }

  return (
    <div className={modalStyles.modalSignInUp} ref={modal}>
      <div className={signInUpStyles.closeBtn} onClick={handleClickCloseBtn} />
      <div className={signInUpStyles.signIuUp}>
        <h1 className={signInUpStyles.title}>{isSignIn ? 'Sign In' : 'Sign Up'}</h1>
        <form method="post" className={signInUpStyles.form} onSubmit={isSignIn ? handleSignIn : handleSignUp}>
          <input type="email" name="email" placeholder="Email" className={signInUpStyles.input} defaultValue={email}
                 required/>
          <input type="password" name="password" placeholder="Password" required
                 className={signInUpStyles.input}/>
          <button type="submit" className={signInUpStyles.button}>{isSignIn ? 'Sign In' : 'Sign Up'}</button>
        </form>
      </div>
    </div>
  );
};
