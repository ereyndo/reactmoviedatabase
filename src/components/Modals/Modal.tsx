import {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import {Nullable} from 'tsHelper';

type ModalProps = { children: JSX.Element | JSX.Element[] };

export const Modal = ({children}: ModalProps) => {
  const [divElement] = useState<HTMLDivElement>(document.createElement('div'));

  useEffect(() => {
      const modalRoot: Nullable<HTMLElement> = document.getElementById('modal-root');
      modalRoot!.appendChild(divElement);

      return () => {
        modalRoot!.removeChild(divElement);
      };
    },
    [divElement]);

  return ReactDOM.createPortal(
    children,
    divElement
  );
};
