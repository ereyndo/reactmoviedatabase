import statusWindowStyles from './statusWindow.module.scss';
import modalStyles from '../modal.module.scss';

type StatusWindowProps = {
  message: string,
  isError: boolean
};

export const StatusWindow = ({message, isError}: StatusWindowProps) => {
  return (
    <div className={modalStyles.modalStatus}>
      <div className={isError ? statusWindowStyles.errorWindow : statusWindowStyles.confirmEmailNotification}>
        {message}
      </div>
    </div>
  );
}
