import React from 'react';
import styles from './footer.module.scss';

type FooterProps = {
    authStatus: boolean
};

export const Footer = ({authStatus}: FooterProps) => {
  return (
    <footer className={styles.footerContainer}>
      <div className={!authStatus ? styles.footerUnauth : styles.footerAuth}>
        <p className={styles.developerName}>Created by Rostyslav</p>
      </div>
    </footer>
  );
};
