import React from 'react';
import {Navbar} from './Navbar';
import {Invitation} from './Invitation';
import styles from './header.module.scss';

type HeaderProps = {
    authStatus: boolean
};

export const Header = ({authStatus}: HeaderProps) => {
  return (
    <div className={!authStatus ? styles.headerUnauth : styles.headerAuth}>
      <Navbar authStatus={authStatus}/>
      {!authStatus ? <Invitation/> : null}
    </div>
  );
};
