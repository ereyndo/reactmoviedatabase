import React from 'react';
import {UserMenuTools} from './UserMenuTools';
import styles from './navbar.module.scss';
import {Link} from 'react-router-dom';
import {useAppDispatch} from 'hooks/reduxHooks';
import {setEmail, setIsSignIn, setIsSignModal} from 'slices/unAuthUserSlice';

type NavbarProps = {
  authStatus: boolean
};

export const Navbar = ({authStatus}: NavbarProps) => {
  const dispatch = useAppDispatch();

  const clickHandler = () => {
    dispatch(setIsSignModal(true));
    dispatch(setEmail(''));
    dispatch(setIsSignIn(true));
  };

  return (
    <div className={!authStatus ? styles.navbarUnauth : styles.navbarAuth}>
      {
        authStatus ?
          <>
            <Link to="/" className={styles.link}>
              <p className={`${styles.logo} ${styles.authLogo}`}>
                <span className={styles.firstLogoLetter}>D</span>
                <span className={styles.restOfLogoLetters}>igicL</span>
              </p>
            </Link>
            <UserMenuTools/>
          </>
          :
          <>
            <p className={styles.logo}>
              <span className={styles.firstLogoLetter}>D</span>
              igicL
            </p>
            <button className={styles.signInBtn} onClick={clickHandler}>Sign In</button>
          </>
      }
    </div>
  );
};
