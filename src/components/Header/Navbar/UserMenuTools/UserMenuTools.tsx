import React, {useRef} from 'react';
import {DropDownMenu} from './DropDownMenu';
import {SearchBar} from './SearchBar';
import {useActivityStatus} from 'hooks/useActivityStatus';
import styles from './userMenuTools.module.scss';
import {Nullable} from 'tsHelper';

export const UserMenuTools = () => {
  const movieCard = useRef<Nullable<HTMLDivElement>>(null);
  const personIcon = useRef<Nullable<HTMLDivElement>>(null);

  const isMenuActive = useActivityStatus(movieCard, personIcon);

  return (
    <div className={styles.userMenuTools}>
      <SearchBar/>
      <div ref={movieCard}>
        <div className={`${styles.personIcon} ${isMenuActive ? styles.personIconActive : null}`} ref={personIcon} />
        {
          isMenuActive ?
            <DropDownMenu/>
            :
            null
        }
      </div>
    </div>
  );
};
