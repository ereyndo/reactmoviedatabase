import React from 'react';
import styles from './dropDownMenu.module.scss';
import {Link} from 'react-router-dom';
import {changeAuthStatusAndCurrentUser, selectCurrentUser} from 'slices/commonSlice';
import {useAppDispatch, useAppSelector} from 'hooks/reduxHooks';

export const DropDownMenu = () => {
  const dispatch = useAppDispatch();
  const currentUser = useAppSelector(selectCurrentUser);

  const handleSignOut = async () : Promise<void> => {
    if (currentUser) {
      try {
        await currentUser.logout();
        dispatch(changeAuthStatusAndCurrentUser(false));
      } catch(error) {
        // have to somehow process an error
      }
    }
  };

  return (
    <div className={styles.dropdown}>
      <ul className={styles.list}>
        <li key='mainList' className={styles.listItem}>
          <Link to='/' className={styles.link}>Main List</Link>
        </li>
        <li key='myList' className={styles.listItem}>
          <Link to='/my-list' className={styles.link}>My List</Link>
        </li>
        <li key='signOut' className={styles.signOut} onClick={handleSignOut}>
          <span className={styles.link}>Sign Out</span>
        </li>
      </ul>
    </div>
  );
};
