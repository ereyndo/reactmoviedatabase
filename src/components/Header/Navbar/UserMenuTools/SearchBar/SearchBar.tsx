import React from 'react';
import styles from './searchBar.module.scss';
import {useNavigate} from 'react-router-dom';

export const SearchBar = () => {
  const navigate = useNavigate();

  const enterHandler = (event: React.KeyboardEvent) => {
    if (event.key === 'Enter') {
      const eventTarget = event.target as HTMLInputElement;
      navigate(`/search/search_query=${eventTarget.value}`);
      eventTarget.value = '';
      eventTarget.blur();
    }
  };

  return (
    <>
      <input type='text' className={styles.searchBar} placeholder='Search for show...' onKeyUp={enterHandler}/>
    </>
  );
};
