import React from 'react';
import {GetStarted} from 'components/GetStarted';
import styles from './invitation.module.scss';

export const Invitation = () => {
  return (
    <div className={styles.invitation}>
      <h1 className={styles.invitationalText}>Dive into <span>the cinematography world</span>.</h1>
      <p className={styles.additionalText}>Enjoy watching anywhere.</p>
      <GetStarted/>
    </div>
  );
};
