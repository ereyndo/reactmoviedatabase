import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {AppDispatch, RootState} from 'store';
import {Nullable} from 'tsHelper';
import GoTrue, {User} from 'gotrue-js';

export const auth = new GoTrue({
  APIUrl: 'https://digicl.netlify.app/.netlify/identity',
  audience: '',
  setCookie: false,
});

interface CommonState {
  authStatus: boolean,
  currentUser: Nullable<User>,
}

const initialState: CommonState = {
  authStatus: !!auth.currentUser(),
  currentUser: auth.currentUser(),
};

export const commonSlice = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setAuthStatus: (state, action: PayloadAction<boolean>) => {
      state.authStatus = action.payload;
    },
    setCurrentUser: (state, action: PayloadAction<Nullable<User>>) => {
      state.currentUser = action.payload;
    },
  }
});

export function changeAuthStatusAndCurrentUser(authStatus: boolean) {
  return function changeAuthStatusAndCurrentUserThunk(dispatch: AppDispatch) {
    dispatch(setAuthStatus(authStatus));
    localStorage.removeItem('basicShowList');
    dispatch(setCurrentUser(auth.currentUser()));
  }
}

export const { setAuthStatus, setCurrentUser } = commonSlice.actions;

export const selectAuthStatus = (state: RootState) => state.common.authStatus;
export const selectCurrentUser = (state: RootState) => state.common.currentUser;

export default commonSlice.reducer;
