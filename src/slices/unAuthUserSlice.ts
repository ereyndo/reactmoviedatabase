import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {RootState} from 'store';

interface UnAuthUserState {
  isSignModal: boolean,
  isInputEmpty: boolean,
  isSignIn: boolean,
  email: string,
}

const initialState: UnAuthUserState = {
  isSignModal: false,
  isInputEmpty: false,
  isSignIn: false,
  email: '',
};

export const unAuthUserSlice = createSlice({
  name: 'unAuthUser',
  initialState,
  reducers: {
    setIsSignModal: (state, action: PayloadAction<boolean>) => {
      state.isSignModal = action.payload;
    },
    setIsInputEmpty: (state, action: PayloadAction<boolean>) => {
      state.isInputEmpty = action.payload;
    },
    setIsSignIn: (state, action: PayloadAction<boolean>) => {
      state.isSignIn = action.payload;
    },
    setEmail: (state, action: PayloadAction<string>) => {
      state.email = action.payload;
    },
  }
});

export const { setIsSignModal, setIsInputEmpty, setIsSignIn, setEmail } = unAuthUserSlice.actions;

export const selectIsSignModal = (state: RootState) => state.unAuthUser.isSignModal;
export const selectIsInputEmpty = (state: RootState) => state.unAuthUser.isInputEmpty;
export const selectIsSignIn = (state: RootState) => state.unAuthUser.isSignIn;
export const selectEmail = (state: RootState) => state.unAuthUser.email;

export default unAuthUserSlice.reducer;
