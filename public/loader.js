const loaderContainer = document.querySelector('.loader-container');
const isLoaderHidden = () => loaderContainer.classList.contains("loader_disabled");

window.addEventListener('load', () => {
  loaderContainer.classList.add("loader_disabled");
});

loaderContainer.addEventListener("transitionend", () => {
  if (isLoaderHidden()) {
    loaderContainer.style.display = "none";
  }
});
